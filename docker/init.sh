#!/bin/bash

set -e

NGINX_CONF="/etc/nginx/sites-available/default"

export LOOPGRAFTER_FRONTEND_PREFIX=$(echo -n /${LOOPGRAFTER_FRONTEND_PREFIX}/ | sed 's#//#/#g')
export LOOPGRAFTER_BACKEND_URL=$(echo -n ${LOOPGRAFTER_BACKEND_URL}/ | sed 's#//$#/#g')

sed -i "s#__version__#${LOOPGRAFTER_VERSION}#g" "$NGINX_CONF"
sed -i "s#__prefix__#${LOOPGRAFTER_FRONTEND_PREFIX:-/}#g" "$NGINX_CONF"
sed -i "s#__backend__#${LOOPGRAFTER_BACKEND_URL:-http://backend:6969/api/}#g" "$NGINX_CONF"

npm run build

exec nginx -g "daemon off;"
