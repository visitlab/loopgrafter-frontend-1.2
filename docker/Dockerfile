FROM node:lts-buster-slim

ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Prague

RUN \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone && \
    apt-get update && \
    apt-get install -y \
        tar \
        gzip \
        nginx-light

COPY nginx_site.conf /etc/nginx/sites-available/default
COPY init.sh /

ENV LOOPGRAFTER_VERSION 1.0.0.0

COPY loopgrafter-frontend-${LOOPGRAFTER_VERSION}.tar.gz /tmp/

RUN \
    tar xzf /tmp/loopgrafter-frontend-${LOOPGRAFTER_VERSION}.tar.gz -C /opt/ && \
    rm /tmp/loopgrafter-frontend-${LOOPGRAFTER_VERSION}.tar.gz && \
    cd /opt/loopgrafter-frontend-${LOOPGRAFTER_VERSION} && \
    npm install && \
    npx browserslist@latest --update-db && \
    \
    # Do a test build to find problems before deployment
    npm run build && \
    rm -fr dist/ && \
    \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*


WORKDIR /opt/loopgrafter-frontend-${LOOPGRAFTER_VERSION}

EXPOSE 80

CMD ["/init.sh"]
