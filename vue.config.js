prefix = process.env.LOOPGRAFTER_FRONTEND_PREFIX || '/'
prefix = `${prefix.startsWith('/') ? '' : '/'}${prefix}${prefix.endsWith('/') ? '' : '/'}`

module.exports = {
  publicPath: prefix,
  configureWebpack: {
    devtool: 'source-map',
    devServer: {
      port: 8080,
      watchFiles: {
        paths: ['src/**/*.vue', 'src/**/*.js'],
        options: {
          usePolling: true
        }
      },
    },
  },
  chainWebpack: config => {
    config.module
      .rule('raw')
      .test(/\.res\.[a-z]*$/)
      .use('raw-loader')
      .loader('raw-loader')
      .end()
  }
}
