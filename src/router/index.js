import Vue from "vue";
import VueRouter from 'vue-router';

import NewJob from "../views/NewJob"
import Pregraft from "../views/Pregraft"
import Postgraft from "../views/Postgraft"
import UseCase from "../views/UseCase"
import Acknowledgement from "../views/Acknowledgement"
import Privacy from "../views/Privacy"
import Terms from "../views/TermsOfUse"
import SolutionProgressbar from "../views/SolutionProgressbar"

Vue.use(VueRouter);

const routes = [
    {
        path: '/', name: 'new',
        meta: {
            reload: true,
        },
        component: NewJob
    },
    {
        path: '/graft-design', name: 'graft-design',
        // meta: {
        //     reload: true,
        // },
        component: Pregraft
    },
    {
        path: '/example', name: 'example',
        meta: {
            reload: true,
            activateExample: true
        },
        component: Pregraft
    },
    {
        path: '/results/check/:jobId',
        name: 'results-check',
        component: SolutionProgressbar,
        props: true

    },
    {
        path: '/results/view/:jobId',
        name: 'results-view',
        component: Postgraft,
        props: true
    },
    {
        path: '/usecase', name: 'usecase',
        meta: {
            reload: false
        },
        component: UseCase
    },
    {
        path: '/acknowledgement', name: 'acknowledgement',
        meta: {
            reload: false
        },
        component: Acknowledgement
    },
    {
        path: '/privacy', name: 'privacy',
        meta: {
            reload: false
        },
        component: Privacy
    },
    {
        path: '/terms', name: 'terms',
        meta: {
            reload: false
        },
        component: Terms
    }
]

export default new VueRouter({
    routes
})