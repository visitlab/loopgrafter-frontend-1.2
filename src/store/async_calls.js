export default async function wait_for_computation_async(api, route, polling_time, taskId = null) {
    let finished = null
    let status_response = null
    console.log(taskId)
    if(taskId != null){
        route = `/${route}/${taskId}/state`
    } else {
        route = `/${route}/state`
    }
    while (finished != true) {
        status_response = await api.get(route)
        await new Promise(resolve => setTimeout(resolve, polling_time));
        finished = status_response.data.finished;
    }
    return status_response
}

