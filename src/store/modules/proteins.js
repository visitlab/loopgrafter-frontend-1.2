import _, { chain, differenceBy, keyBy, max, zip, zipWith } from 'lodash';
import Protein from "./models/protein";
import { Segmentation, SegmentationType } from "./models/segmentation";
import Loop from "./models/loop";
import wait_for_computation_async from "../async_calls"


const readFile = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsText(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
});

function roundToTwo(num) {
    return +(Math.round(num + "e+2") + "e-2");
}

function getRoleState(state, role){
    return role == 'scaffold' 
        ? state.scaffoldData
        : state.insertData
}

export default {
    state: {
        scaffoldData: {},
        insertData: {}
    },
    getters: {
        getAllLoops: (state, getters) => {
            let loops = []
            loops.push(...getters.getScaffold.loops, ...getters.getInsert.loops);

            return loops;
        },

        getZoomedLoop(state) {
            return state.zoomedLoop;
        },

        getMaxCcorWithActive: (state, getters) => (loop_id, strategy) => {
            let loop = getters.getScaffold.loops.find(l => l.id == loop_id);
            let loops_of_interest = getters.getWantedLoops;
            let ccor_values = []
            for (let interesting_loop_id of loops_of_interest.map(l => l.id)) {
                if (interesting_loop_id != loop_id) {
                    ccor_values.push(Math.abs(strategy(loop, interesting_loop_id)))
                }
            }
            return max(ccor_values)
        },

        getScaffold(state) {
            return state.scaffoldData;
        },

        getInsert(state) {
            return state.insertData;
        },

        getRole: (state, getters) => (role) => {
            return role == "scaffold" ? getters.getScaffold : getters.getInsert
        }


    },
    mutations: {
        setScaffoldData(state, protein) {
            state.scaffoldData = protein
        },

        setInsertData(state, protein) {
            state.insertData = protein
        },

        setRoleData(state, {role, protein}) {
            if(role == 'scaffold'){
                state.scaffoldData = protein
            } else {
                state.insertData = protein
            }
        },

        addCustomLoop(state, { role, loop }) {
            let protein = getRoleState(state, role);
            protein.customLoops.push(loop);
        },
        addComuptedLoop(state, { role, loop }) {
            let protein = getRoleState(state, role);
            protein.computedLoops.push(loop);
        },

        zoomToLoop(state, { role, loop }) {
            let protein = getRoleState(state, role);
            protein.zoomedLoop = loop;
        },

        invalidateLoopsGeometry(state, { protein, from, to }) {
            protein.invalidateLoopsGeometry(from, to)

        },

        setLoopPosition(state, { protein, loop, start, end }) {
            loop.setPosition(start, end, protein);
        },

        addLoopFlexibility(state, { role, loop_id, flexibility, crosscorelation }) {
            let protein = getRoleState(state, role);
            let loop = protein.loops.find(l => l.id == loop_id);
            if (loop == undefined) {
                console.warn("Loop does not exist - probably superloop?", loop_id);
                return
            }
            loop.flexibility = flexibility;
            loop.crosscorelation = crosscorelation;

        },

        addProteinFlexibilityTypes(state, { role, flexibility }) {
            let protein = getRoleState(state, role);
            protein.availableBfactors
                .push(...flexibility.types);
            protein.bfactorCorrelation = flexibility.correlation_matrix;
        },

        addProteinCcorsTypes(state, { role, ccors }) {
            let protein = getRoleState(state, role);
            protein.availableCcors
                .push(...ccors);

        },

        addProteinSecondaryStructureFlexibility(state, { role, flexibility }) {
            let protein = getRoleState(state, role);

            for (let flex of flexibility.values) {
                let bfactors = {};
                for (let flextype of flexibility.types) {
                    bfactors[flextype] = roundToTwo(parseFloat(flex[flextype]))
                    if (bfactors[flextype] == null) {
                        console.error("there are nulls for some of the secondary structure flexibilities", protein.name, flexibility, flex, flextype)
                    }
                }
                let segment = protein.segmentation.segments.find(s => s.name == flex.segment);
                if (segment == null) {
                    console.error("Unknown protein ss segment")
                }
                for (let acid_num = segment.from; acid_num <= segment.to; acid_num++) {
                    let acid = protein.getAcid(acid_num);
                    if (acid != null) {
                        acid.bfactors.secondary_structure = bfactors;
                    }
                }
            }

        },

        addProteinSecondaryStructureCrosscol(state, { role, crosscol }) {
            let protein = getRoleState(state, role);
            protein.secs_crosscol = {};
            for (let ccrosc_type in crosscol) {
                protein.secs_crosscol[ccrosc_type] = {};


                for (let secs in crosscol[ccrosc_type].segment_dictionary) {
                    let pairings = zipWith(
                        crosscol[ccrosc_type].segment_dictionary[secs],
                        crosscol[ccrosc_type].segment_names,
                        (value, secsId) => ({ secsId, value: roundToTwo(Number(value)) })
                    )
                    protein.secs_crosscol[ccrosc_type][secs] = keyBy(pairings, c => c.secsId);

                }
            }

        },
        addAcidsFlexibility(state, { role, flexibility }) {
            let protein = getRoleState(state, role);

            for (let flex of flexibility.values) {
                let acid = protein.getAcid(parseInt(flex.segment));
                let bfactors = {};

                for (let flextype of flexibility.types) {
                    bfactors[flextype] = roundToTwo(parseFloat(flex[flextype]))
                }
                if (acid != null) {
                    acid.bfactors.residue = bfactors;
                }
            }

        },

        setSecondaryStructures(state, { role, from, to, type }) {
            let protein = getRoleState(state, role);
            let acids = protein.getAcids(from.pdb_number, to.pdb_number);
            for (let acid of acids) {
                acid.secondary_structure = type
            }
        },

        deleteLoop(state, { role, loop }) {
            let protein = getRoleState(state, role);
            protein.computedLoops = protein.computedLoops.filter(l => l.id != loop.id);
            protein.customLoops = protein.customLoops.filter(l => l.id != loop.id);

        },

        renewSegmentation(state, { role }) {
            let protein = getRoleState(state, role);
            protein.segmentation = new Segmentation(protein);
        }

    },
    actions: {
        deleteLoops(context, { role }) {
            let protein = context.getters.getRole(role);
            for (let loop of protein.loops) {
                context.dispatch("deleteLoop", { role, loop })
            }
            for (let loop of protein.superloops) {
                context.dispatch("deleteLoop", { role, loop })
            }
            context.dispatch("nukeLoopExploartionData", { role })

        },
        purgeNoncustomLoops(context, { role }) {
            let protein = context.getters.getRole(role);
            for (let loop of protein.computedLoops) {
                context.dispatch("deleteLoop", { role, loop })
            }
        },

        async deleteSingleLoop(context, { role, loop }) {
            let protein = context.getters.getRole(role);
            let deletionRequest = context.getters.getApi.delete(`/loop/${protein.name}/${loop.chain}/${loop.id}`);
            context.dispatch("addAwaitable", {
                id: _.uniqueId("awaiting"),
                promise: deletionRequest,
                description: `Removing loop ${loop.id}`
            });
            try {
                let deletion = await deletionRequest;
                if (deletion.data.run_OK != true) {
                    throw Error(deletion.data.err_msg);
                }
            } catch (err) {
                context.dispatch("setErrorMessage", { error: err });
                throw err;
            }
            context.dispatch("deleteLoop", { role, loop })
        },
        deleteLoop(context, { role, loop }) {

            context.commit("deleteLoop", { role, loop });
            context.commit("deleteInterestingLoop", loop);
            context.dispatch("removeLoopDetailByLoop", loop);

            if (role == "scaffold") {
                context.dispatch("nukeFlexibility");
            }
            context.dispatch("nukePairing");
        },

        setSecondaryStructures(context, { role, from, to, type }) {
            context.commit("setSecondaryStructures", { role, from, to, type });
            context.dispatch("nukeLoopExploartionData", { role })
        },

        async addCustomLoop(context, { role, region }) {
            let protein = context.getters.getRole(role);

            while (protein.allLoops.map(l => l.id).includes(region.name)) {
                region.name = region.name + "'";
            }
            let from = region.from.pdb_number;
            let to = region.to.pdb_number;

            while (protein.getAcid(from).simple_secondary_structure == "C"
                && from < to) {
                from += 1;
            }

            while (protein.getAcid(to).simple_secondary_structure == "C"
                && from < to) {
                to -= 1;
            }

            let newLoopPromise = context.getters.getApi.post("/loop", {
                protein_name: protein.name, 
                chain: protein.chain,
                start: from,
                end: to
            })
            context.dispatch("addAwaitable", {
                id: _.uniqueId("awaiting"),
                promise: newLoopPromise,
                description: `Adding new loop from ${from} to ${to}`
            });
            try {
                let newLoopData = await newLoopPromise;
                if (newLoopData.data.run_OK != true) {
                    throw Error(newLoopData.data.err_msg);
                }

                let loop_info = newLoopData.data.loop_info;
                let geometry = {
                    distance: loop_info.distance,
                    rho: loop_info.rho,
                    delta: loop_info.delta,
                    theta: loop_info.theta
                }
                let id = loop_info.id;


                let newLoop = new Loop(id,
                    region.name,
                    from,
                    to,
                    protein.getAcids(from, to),
                    to - from,
                    protein.name,
                    protein.chain,
                    protein,
                    "Computed",
                    geometry);
                context.dispatch("nukeFlexibility", { role: protein.role })
                context.commit("addComuptedLoop", { role: protein.role, loop: newLoop })
                context.dispatch("makeInterestingLoop", { rawLoop: newLoop })
            } catch (err) {
                context.dispatch("setErrorMessage", { error: err });
                throw err;
            }
        },


        addProtein(context, { proteinName, chain, role, superimposed }) {
            let api = context.getters.getApi;
            chain = chain.toUpperCase();

            let pdbPromise = api.get(`/downloadpdbs/${proteinName.toLowerCase()}${superimposed ? "_super" : ""}`)

            let dsspPromise = api.get(`/dssp/${proteinName}`)


            context.dispatch("addAwaitable", {
                id: _.uniqueId("awaiting"),
                promise: dsspPromise,
                description: "Computing DSSP assingment"
            });

            context.dispatch("addAwaitable", {
                id: _.uniqueId("awaiting"),
                promise: pdbPromise,
                description: "Getting PDB"
            });

            return Promise.all([pdbPromise, dsspPromise])
                .then(function ([pdb, dssp]) {
                    let protein = new Protein(proteinName, pdb.data, dssp.data, chain, role);
                    context.commit("setRoleData", {role, protein});
                    debugger;
                    return protein
                })
        },

        addRCBSProtein(context, { name, chain }) {
            let request = context.getters.getApi
                .post(`/rcsb`, { "protein": name.trim(), "chain": chain })
                .catch(error => {
                    // the other choice is figure out what the hell am I doing in App.vue, and I'm way too beyond this project to do that
                    error.response.data.message = `[${name}:${chain}]  ${error.response.data.message}`;
                    throw error;
                });
            context.dispatch("addAwaitable", {
                id: _.uniqueId("awaiting"),
                promise: request,
                description: "Downloading from RCSB"
            });
            return request;
        },

        uploadProtein(context, { name, chain, file }) {
            let request = readFile(file)
                .then(encodedFile => context.getters.getApi.post(`/userpdb`, {
                    pdbname: name,
                    chain: chain,
                    pdbfile: encodedFile
                })).catch(error => {
                    error.response.data.message = `[${name}:${chain}]  ${error.response.data.message}`;
                    throw error;
                });
            context.dispatch("addAwaitable", {
                id: _.uniqueId("awaiting"),
                promise: request,
                description: "Uploading custom PDB file"
            });
            return request
        },


        async computeFlexibility(context, { protein, bfactors, files }) {
            context.commit("renewSegmentation", { role: protein.role});
            let flexibility_files = await Promise.all(files.map(f => readFile(f)));
            let flexibility_computation = Promise.all(
                [
                    (() => {
                        let residue_proimise = context.getters.getApi.post(`/flexibility/submit`, {
                            pdb: protein.name,
                            chain: protein.chain,
                            types: bfactors,
                            file: flexibility_files,
                            bfactor_aggregation_type: 'residue',
                        });
                        return residue_proimise.then(
                            (response) => wait_for_computation_async(context.getters.getApi, "flexibility", 5000, response.data.taskId)
                        )
                    })(),
                    (() => {
                        let loops_promise = context.getters.getApi.post(`/flexibility/submit`, {
                            pdb: protein.name,
                            chain: protein.chain,
                            types: bfactors,
                            file: flexibility_files,
                            bfactor_aggregation_type: 'loop',
                        });
                        return loops_promise.then(
                            (response) => wait_for_computation_async(context.getters.getApi, "flexibility", 5000, response.data.taskId)
                        )
                    })(),
                    (() => {
                        let secs_promise = context.getters.getApi.post(`/flexibility/submit`, {
                            pdb: protein.name,
                            chain: protein.chain,
                            types: bfactors,
                            file: flexibility_files,
                            bfactor_aggregation_type: 'secs'
                        });
                        return secs_promise.then(
                            (response) => wait_for_computation_async(context.getters.getApi, "flexibility", 5000, response.data.taskId)
                        )
                    })()
                ]
            ).then(([residue_response, loops_response, secs_response]) => {
                if (!residue_response.data.canBeRetrieved) {
                    throw new Error("Computation of flexibility per-residue has failed.")
                }
                if (!loops_response.data.canBeRetrieved) {
                    throw new Error("Computation of flexibility per-loop has failed.")
                }
                if (!secs_response.data.canBeRetrieved) {
                    throw new Error("Computation of flexibility per-secs has failed.")
                }

                return Promise.all([
                    context.getters.getApi.get(`/flexibility/${residue_response.data.taskId}/results`),
                    context.getters.getApi.get(`/flexibility/${loops_response.data.taskId}/results`),
                    context.getters.getApi.get(`/flexibility/${secs_response.data.taskId}/results`),
                ])

            }).then((([residues, loops, secs]) => {
                //residues
                context.commit("addAcidsFlexibility", {
                    role: protein.role,
                    flexibility: residues.data
                })
                //loops
                for (let flex of loops.data.values) {
                    let flexibility_dict = {}
                    for (let flextype of loops.data.types) {
                        flexibility_dict[flextype] = roundToTwo(parseFloat(flex[flextype]))
                    }

                    let crosscorelation_types = Object.keys(loops.data.cross_correlation);
                    let ccors = {}
                    for (let type of crosscorelation_types) {
                        let ccors_dictionary = {};
                        let ccors_segments = loops.data.cross_correlation[type].segment_names;
                        ccors_dictionary = zipWith(
                            loops.data.cross_correlation[type].segment_dictionary[flex.segment],
                            ccors_segments,
                            (value, loopId) => ({ loopId, value: roundToTwo(parseFloat(value)) })
                        )
                        ccors[type] = keyBy(ccors_dictionary, c => c.loopId);
                    }
                    context.commit("addLoopFlexibility", {
                        role: protein.role,
                        loop_id: flex.segment,
                        flexibility: flexibility_dict,
                        crosscorelation: ccors
                    })
                    context.commit("addProteinCcorsTypes", {
                        role: protein.role,
                        ccors: crosscorelation_types
                    })
                }

                context.commit("addProteinFlexibilityTypes", {
                    role: protein.role,
                    flexibility: loops.data
                })
                //Secs
                context.commit("addProteinSecondaryStructureCrosscol", {
                    role: protein.role,
                    crosscol: secs.data.cross_correlation
                });
                context.commit("addProteinSecondaryStructureFlexibility", {
                    role: protein.role,
                    flexibility: secs.data
                })
            }))

            context.dispatch("addAwaitable", {
                id: _.uniqueId("awaiting"),
                promise: flexibility_computation,
                description: `Computing flexibility for ${protein.displayName}`
            });
            return flexibility_computation;
        },

        addLoops(context, { role, geometries, source }) {
            let protein = context.getters.getRole(role)
            let displayName = protein.displayName;
            for (let [index, rawLoop] of _.sortBy(geometries.loops, l => l.start).entries()) {
                let loop = new Loop(
                    rawLoop.id,
                    `${displayName}_${rawLoop.chain}_${index + 1}`,
                    rawLoop.start,
                    rawLoop.end,
                    protein.getAcids(rawLoop.start, rawLoop.end),
                    rawLoop.length,
                    rawLoop.pdb,
                    rawLoop.chain,
                    protein,
                    source,
                    {
                        distance: rawLoop.distance,
                        delta: rawLoop.delta,
                        rho: rawLoop.rho,
                        theta: rawLoop.theta
                    }
                )
                context.commit("addComuptedLoop", {
                    role: protein.role,
                    loop
                })
                context.dispatch("makeInterestingLoop", { rawLoop: loop })
            }
        },

        computeLoops(context, {role}) {
            let protein = context.getters.getRole(role)
            let request = context.getters.getApi.post(`/loops/submit`, {
                pdb: protein.name,
                chain: protein.chain,
                segmentation: new Segmentation(protein)
                    .segments
                    .filter(segment => segment.type == SegmentationType.secondary_structure)
            }).then((response) => {   //check until finished
                return Promise.all([wait_for_computation_async(context.getters.getApi, "loops", 5000, response.data.taskId), response.data.taskId])
            }).then(([response, taskId]) => {
                if (!response.data.canBeRetrieved) {
                    throw new Error("Loops computation failed with unknown error.");
                }
                return context.getters.getApi.get(`/loops/${taskId}/results`)
            }).then((response) => { //set
                context.dispatch("purgeNoncustomLoops", { role: protein.role });
                context.dispatch("addLoops", { role: protein.role, geometries: response.data, source: "Computed" })
            })
            context.dispatch("addAwaitable", {
                id: _.uniqueId("awaiting"),
                promise: request,
                description: `Computing loops for ${protein.role} ${protein.displayName}`
            });
            return request;
        },

        zoomToLoop(context, { role, id }) {
            context.commit("zoomToLoop", { role, loop: id });
        },
        removeRole(context, role) {
            context.commit('setRoleData', {role, protein: null})
        },

    },

}

