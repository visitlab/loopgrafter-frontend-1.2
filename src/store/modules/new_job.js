import _, { get } from "lodash";
import wait_for_computation_async from "../async_calls"

export default {

    state: {
        proposedInsert: null,
        proposedScaffold: null,
        jobDetail: null,
        licenseType: null

    },
    getters: {
        isReadyToBeginGraftDesign: state => {
            return state.proposedInsert != null && state.proposedScaffold != null;
        },
        getProposedInsert: state => state.proposedInsert,
        getProposedScaffold: state => state.proposedScaffold,
        getJobDetail: state => state.jobDetail,
        getLicense: state => state.licenseType
    },
    mutations: {
        proposeInsert: (state, insertInfo) => {
            state.proposedInsert = insertInfo;
        },
        proposeScaffold: (state, scaffoldInfo) => {
            state.proposedScaffold = scaffoldInfo;
        },
        setInfo(state, { jobDetail, licenseType }) {
            state.jobDetail = jobDetail;
            state.licenseType = licenseType;
        }

    },
    actions: {
        proposeProtein: ({ commit }, { proteinInfo, role }) => {
            if (role == "insert") {
                commit("proposeInsert", proteinInfo)
            }
            if (role == "scaffold") {
                commit("proposeScaffold", proteinInfo)
            }
        },

        resolveProposedProtein: async ({ dispatch }, { proteinInfo, role }) => {
            let protein = {
                ...proteinInfo,
                role
            }
            if (proteinInfo.method == "upload") {
                await dispatch("uploadProtein", protein);
            }
            if (proteinInfo.method == "rcsb") {
                await dispatch("addRCBSProtein", protein);
            }
        },

        superimpose({ getters, dispatch }) {
            let superimposition = getters.getApi.post(`/superimposition/submit`, {
                static_name: getters.getProposedScaffold.name,
                static_chain: getters.getProposedScaffold.chain,
                mobile_name: getters.getProposedInsert.name,
                mobile_chain: getters.getProposedInsert.chain
            }).then(
                task => wait_for_computation_async(getters.getApi, "superimposition", 1000, task.data.taskId)
            )

            dispatch("addAwaitable", {
                id: _.uniqueId("awaiting"),
                promise: superimposition,
                description: "Aligning proteins"
            });

            return superimposition;
        },

        async initializeExample({ dispatch }) {
            dispatch("proposeProtein", {
                proteinInfo: {
                    method: "rcsb",
                    name: "6g75",
                    chain: "A",
                },
                role: "scaffold"
            })
            dispatch("proposeProtein", {
                proteinInfo: {
                    method: "rcsb",
                    name: "2psf",
                    chain: "B",
                },
                role: "insert"
            })
            await dispatch("resolveProposedProteins", {
                license: "FULL",
                jobDescription: "",
                email: ""
            })
        },

        async resolveProposedProteins({ getters, dispatch }, job) {
            if (!getters.isReadyToBeginGraftDesign) {
                console.error("scaffold", getters.getProposedScaffold);
                console.error("insert", getters.getProposedInsert)
                throw "You need to choose both the insert and scaffold protein."
            }

            await dispatch("getToken", job)
            let resolveInsert = dispatch("resolveProposedProtein", { proteinInfo: getters.getProposedInsert, role: "insert" });
            let resolveScaffold = dispatch("resolveProposedProtein", { proteinInfo: getters.getProposedScaffold, role: "scaffold" });
            await Promise.all([resolveInsert, resolveScaffold]);
            await dispatch("superimpose")
            let addScaffold = dispatch("addProtein", {
                proteinName: getters.getProposedScaffold.name,
                chain: getters.getProposedScaffold.chain,
                role: "scaffold"
            })
            let addInsert = dispatch("addProtein", {
                proteinName: getters.getProposedInsert.name,
                chain: getters.getProposedInsert.chain,
                role: "insert",
                superimposed: true
            })
            await dispatch("fetchJobInfo");
            return Promise.all([addScaffold, addInsert])

        },

        async fetchJobInfo(context) {
            let { data } = await context.getters.getApi.get(`/session`);
            context.commit('setInfo', { jobDetail: data.title, licenseType: data.calculation_mode })
        },

        
    }
}