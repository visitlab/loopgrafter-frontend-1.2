export default {
    state: {
        errorCount: 0,
        errorMessages: [],
    },
    getters: {
        getErrorRepresentation(state) {
            return state.errorMessages.join("\n\n");
        },
        getErrorCount(state) {
            return state.errorCount;
        }
    },
    mutations: {
        setErrorMessage(state, { error }) {
            state.errorMessages.unshift(`${error}`);
            state.errorCount += 1;
        },
        clearErrorMessages(state) {
            state.errorMessages = [];
        }
    },
    actions: {
        setErrorMessage(context, errorMessage) {
            debugger;

            if (errorMessage.error.ignoreUnhandledRejection) {
                return
            }
            context.commit('setErrorMessage', { error: errorMessage.error });
            console.error(`Displaying error message for:\n`, errorMessage);
            // throw { source: errorMessage, ignoreUnhandledRejection: true };
        },

        clearErrorMessages(context) {
            context.commit('clearErrorMessages');
        }
    }
}