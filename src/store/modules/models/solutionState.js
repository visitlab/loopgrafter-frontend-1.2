export default class SolutionState {
    constructor(status, order, finished, error, canBeRetrieved) {
        this.id = status;
        this.order = order;
        this.finished = finished;
        this.error = error;
        this.canBeRetrieved = canBeRetrieved;
    }

    isCurrent(state_id) {
        return this.id == state_id;
    }
    isDone(order) {
        return this.order > order;
    }
    isError(state_id) {
        return this.id == state_id + "_FAILED"
    }
}