export default class Solution {
    constructor(name, order, dope, rosetta, replacments, sequence) {
        this.name = name;
        this.order = order;
        this.dope = dope;
        this.rosetta = rosetta;
        this.replacments = replacments;
        this.raw_sequence = sequence;
    }

    get sequencePreview() {
        let isUpperCase = (string) => /^[A-Z]*$/.test(string)
        let preview = []

        for (let acid of this.raw_sequence) {
            preview.push(isUpperCase(acid) ? "S" : "I")
        }
        return preview;
    }


}